package gui;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import utils.ManagerSession;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;

public class DatosPanel extends JPanel {

	private Principal principal;
	private JTextField txtNombre;
	private JTextField txtClave;
	private JLabel lblRut;

	public DatosPanel(Principal principal) {
		this();
		this.principal = principal;
	}
	
	public DatosPanel() {
		setBounds(0, 0, Principal.posX, Principal.posY);
		setLayout(null);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verHomePanel();
			}
		});
		btnVolver.setBounds(12, 12, 117, 25);
		add(btnVolver);
		
		JLabel lblRut2 = new JLabel("Rut:");
		lblRut2.setBounds(22, 60, 70, 15);
		add(lblRut2);
		
		lblRut = new JLabel("12.875.864-6");
		lblRut.setBounds(104, 60, 127, 15);
		add(lblRut);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(22, 99, 70, 15);
		add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(104, 97, 114, 19);
		add(txtNombre);
		txtNombre.setColumns(10);
		
		txtClave = new JTextField();
		txtClave.setBounds(104, 128, 114, 19);
		add(txtClave);
		txtClave.setColumns(10);
		
		JLabel lblClave = new JLabel("Clave:");
		lblClave.setBounds(22, 130, 70, 15);
		add(lblClave);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario =(Usuario)ManagerSession.findObject("usuario");
				Usuario user = new Usuario();
				user.setRut(usuario.getRut());
				user.setPin(txtClave.getText());
				user.setNombre(txtNombre.getText());
				ManagerSession.saveObject("usuario",user);
				UsuarioController usuarioController = new UsuarioController();
							
			}
		});
		btnGuardar.setBounds(45, 167, 117, 25);
		add(btnGuardar);

	}
	
	public void init(){
		Usuario usuario =(Usuario)ManagerSession.findObject("usuario");
		lblRut.setText(usuario.getRut());
		txtNombre.setText(usuario.getNombre());
		txtClave.setText(usuario.getPin());
		
	}
}
