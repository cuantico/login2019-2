package gui;

import javax.swing.JPanel;
import java.util.Calendar;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import utils.ManagerSession;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextPane;
import java.util.Calendar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
public class SolicitudPanel extends JPanel {

	private Principal principal;
	private JTextField txtMonto;
	private JTextField txtMateria;
	private JTextField txtPersonas;
	private JTextField txtDia;
	private JTextField txtMes;

	public SolicitudPanel(Principal principal){
		this();
		this.principal = principal;
	}
	public SolicitudPanel() {
		setBackground(new Color(72, 209, 204));
		setBounds(0,0,262,376);
		setLayout(null);

		JButton btnSolicitarAyuda2 = new JButton("Solicitar Ayuda");
		btnSolicitarAyuda2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ayudantia ayudantia = new Ayudantia();
				ayudantia.setMateria(txtMateria.getText());
				ayudantia.setN_personas(txtPersonas.getText());
				ayudantia.setOferta(txtMonto.getText());
				
				principal.verHomePanel();
				
			}
		});
		
		btnSolicitarAyuda2.setBounds(65, 202, 124, 42);
		add(btnSolicitarAyuda2);
		
		JLabel lblMateria = new JLabel("Materia:");
		lblMateria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMateria.setBounds(21, 34, 102, 14);
		add(lblMateria);
		
		JLabel lblFecha = new JLabel("Fecha ayudantia");
		lblFecha.setVerifyInputWhenFocusTarget(false);
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblFecha.setBounds(21, 88, 94, 14);
		add(lblFecha);
		
		
		
		JLabel lblFechaActual = new JLabel("Fecha actual:");
		lblFechaActual.setBounds(21, 63, 102, 14);
		add(lblFechaActual);
		
		Calendar c= Calendar.getInstance();
		
		JLabel lblNewLabel = new JLabel(""+Integer.toString(c.get(Calendar.DATE)));
		lblNewLabel.setBounds(133, 63, 20, 14);
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("/ "+Integer.toString(c.get(Calendar.MONTH)));
		lblNewLabel_1.setBounds(154, 63, 20, 14);
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("/ "+Integer.toString(c.get(Calendar.YEAR)));
		lblNewLabel_2.setBounds(174, 63, 47, 14);
		add(lblNewLabel_2);
		
		JLabel lblMontoclp = new JLabel("Monto [CLP]");
		lblMontoclp.setBounds(21, 120, 102, 14);
		add(lblMontoclp);
		
		txtMonto = new JTextField();
		txtMonto.setBounds(125, 117, 86, 20);
		add(txtMonto);
		txtMonto.setColumns(10);
		
		txtMateria = new JTextField();
		txtMateria.setBounds(125, 32, 86, 20);
		add(txtMateria);
		txtMateria.setColumns(10);
		
		JLabel lblNpersonas = new JLabel("N\u00B0personas:");
		lblNpersonas.setBounds(21, 145, 86, 14);
		add(lblNpersonas);
		
		txtPersonas = new JTextField();
		txtPersonas.setBounds(125, 142, 86, 20);
		add(txtPersonas);
		txtPersonas.setColumns(10);
		
		txtDia = new JTextField();
		txtDia.setBounds(125, 86, 40, 20);
		add(txtDia);
		txtDia.setColumns(10);
		
		txtMes = new JTextField();
		txtMes.setBounds(184, 86, 40, 20);
		add(txtMes);
		txtMes.setColumns(10);

	}
	public void init(){
		//	txtClave.setText("");
		//	txtRut.setText("");
	}
}
