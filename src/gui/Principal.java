package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import utils.ManagerSession;

import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	private JPanel contentPane;

	public static final int posX = 450;
	public static final int posY = 300;

	private LoginPanel loginPanel;
	private HomePanel homePanel;
	private DatosPanel datosPanel;
	private PinPanel pinPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Principal frame = new Principal();
		frame.setVisible(true);
	}

	/**
	 * Create the frame.
	 */
	//atolondrado
	public Principal() {
		System.out.println("::Cliente ::");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, posX, posY);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		homePanel = new HomePanel(this);
		contentPane.add(homePanel);

		loginPanel = new LoginPanel(this);
		contentPane.add(loginPanel);

		datosPanel = new DatosPanel(this);
		contentPane.add(datosPanel);

		pinPanel = new PinPanel(this);
		contentPane.add(pinPanel);

		UsuarioController usuarioController = new UsuarioController();
		Usuario usuario = usuarioController.findFirst();

		if(usuario != null)
			usuario = usuarioController.identificar(usuario);
		if(usuario == null)
			verLoginPanel();
		else{
			ManagerSession.saveObject("usuario", usuario);
			verHomePanel();
		}

	}

	public void verHomePanel() {
		loginPanel.setVisible(false);
		datosPanel.setVisible(false);
		pinPanel.setVisible(false);
		homePanel.init();
		homePanel.setVisible(true);
	}

	public void verLoginPanel() {
		homePanel.setVisible(false);
		datosPanel.setVisible(false);
		pinPanel.setVisible(false);
		loginPanel.init();
		loginPanel.setVisible(true);
	}
	public void verDatosPanel() {
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		pinPanel.setVisible(false);
		datosPanel.init();
		datosPanel.setVisible(true);
	}
	public void verPinPanel() {
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		datosPanel.setVisible(false);
		pinPanel.init();
		pinPanel.setVisible(true);
	}
}
