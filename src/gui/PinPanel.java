package gui;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import utils.ManagerSession;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;

public class PinPanel extends JPanel {

	private Principal principal;
	private JTextField txtPin;

	public PinPanel(Principal principal) {
		this();
		this.principal = principal;
	}

	public PinPanel() {
		setBounds(0, 0, Principal.posX, Principal.posY);
		setLayout(null);

		JLabel lblNombre = new JLabel("Pin:");
		lblNombre.setBounds(99, 99, 70, 15);
		add(lblNombre);

		txtPin = new JTextField();
		txtPin.setBounds(187, 97, 114, 19);
		add(txtPin);
		txtPin.setColumns(10);

		JButton btnRegistro = new JButton("Registro");
		btnRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario user = new Usuario();
				user.setPin(txtPin.getText());
				UsuarioController usuarioController = new UsuarioController();
				user = usuarioController.actualizarPin(user);
				user = usuarioController.identificar(user);
				if(user == null){
					principal.verLoginPanel();
				}
				else{
					usuarioController.guardar(user);
					principal.verHomePanel();
				}
			}
		});
		btnRegistro.setBounds(147, 128, 117, 25);
		add(btnRegistro);

	}

	public void init(){
		txtPin.setText("");
	}
}
