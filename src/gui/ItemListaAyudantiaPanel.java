package gui;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;

import cl.ufro.bd.Ayudantia;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ItemListaAyudantiaPanel extends JPanel {
private Ayudantia ayudantia;
	public ItemListaAyudantiaPanel(Ayudantia a) {
		this.ayudantia = a;
		setBackground(SystemColor.controlHighlight);
		setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		setLayout(null);
		setSize(290,35);
		JButton iLAPbtn = new JButton("Entrar");
		
		iLAPbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Ver ayudantia con Id: "+ayudantia.getId());
			}
		});
		iLAPbtn.setForeground(new Color(255, 255, 255));
		iLAPbtn.setBackground(new Color(0, 102, 204));
		iLAPbtn.setBounds(201, 8, 89, 18);
		add(iLAPbtn);
		
		JLabel iLAPlblprecio = new JLabel("$"+ayudantia.getPrecio());
		iLAPlblprecio.setForeground(new Color(0, 0, 0));
		iLAPlblprecio.setBounds(27, 10, 46, 14);
		add(iLAPlblprecio);
		
		JLabel iLAPlblfecha = new JLabel(ayudantia.getFecha());
		iLAPlblfecha.setBackground(SystemColor.inactiveCaption);
		iLAPlblfecha.setForeground(new Color(0, 0, 0));
		iLAPlblfecha.setBounds(115, 10, 70, 14);
		add(iLAPlblfecha);

	}
}
