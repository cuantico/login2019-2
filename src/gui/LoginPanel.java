package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import utils.ManagerSession;

import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginPanel extends JPanel {
	private JTextField txtCorreo;
	
	private Principal principal;

	/**
	 * Create the panel.
	 */
	public LoginPanel(Principal principal){
		this();
		this.principal = principal;
	}
	public LoginPanel() {
		setBounds(0, 0, Principal.posX, Principal.posY);
		setLayout(null);
		
		JLabel lblCorreo = new JLabel("Correo:");
		lblCorreo.setBounds(55, 83, 70, 15);
		add(lblCorreo);
		
		JLabel lblufromailcl = new JLabel("@ufromail.cl");
		lblufromailcl.setBounds(237, 83, 145, 15);
		add(lblufromailcl);
		
		txtCorreo = new JTextField();
		txtCorreo.setColumns(10);
		txtCorreo.setBounds(122, 81, 114, 19);
		add(txtCorreo);
		
		JButton btnSiguiente = new JButton("Siguiente");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario();
				usuario.setCorreo(txtCorreo.getText()+"@ufromail.cl");
				
				UsuarioController usuarioController = new UsuarioController();
				usuarioController.registrar(usuario);
				
				ManagerSession.saveObject("usuario", usuario);
				principal.verPinPanel();
			}
		});
		btnSiguiente.setBounds(122, 123, 117, 25);
		add(btnSiguiente);

	}
	public void init() {
		txtCorreo.setText("");
		
		//txtClave.setText("");
		
	}
}
