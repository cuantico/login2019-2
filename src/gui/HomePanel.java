package gui;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JLabel;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.AsignaturaController;
import cl.ufro.controller.UsuarioController;

import utils.ManagerSession;
import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JComboBox;

public class HomePanel extends JPanel {


	private JLabel lblSaludo;
	private Principal principal;
	private JPanel pnlLista;
	private JComboBox hpBox;
	
	public HomePanel(Principal principal) {
		this();
		this.principal = principal;
	}
	
	public HomePanel() {
		setBounds(0, 0, Principal.posX, Principal.posY);
		setLayout(null);
		
		lblSaludo = new JLabel("Hola");
		lblSaludo.setBounds(12, 12, 163, 15);
		add(lblSaludo);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verLoginPanel();
			
			}
		});
		btnSalir.setBounds(286, 7, 117, 25);
		add(btnSalir);
		
		JButton btnDatos = new JButton("Datos");
		btnDatos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verDatosPanel();
			}
		});
		btnDatos.setBounds(286, 49, 117, 25);
		add(btnDatos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 90, 300, 199);
		add(scrollPane);
		
		pnlLista = new JPanel();
		scrollPane.setViewportView(pnlLista);
		pnlLista.setLayout(null);
		
		JButton hpbtnBuscar = new JButton("Buscar");
		hpbtnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = hpBox.getSelectedIndex();
				List<Asignatura> lista = (List<Asignatura>)ManagerSession.findObject("asignaturas");
				System.out.println(lista.get(i).getNombre());
			}
		});
		hpbtnBuscar.setBounds(12, 50, 89, 23);
		add(hpbtnBuscar);
		
		hpBox = new JComboBox();
		hpBox.setBounds(104, 51, 172, 20);
		add(hpBox);

	}
	
	public void init(){
		UsuarioController usuarioController = new UsuarioController();
		Usuario usuario = usuarioController.getUsuarioSession();
		lblSaludo.setText("Hola "+usuario.getNombre());
		
		AsignaturaController asignaturaController = new AsignaturaController();
		List<Asignatura> lista = asignaturaController.listar();
		DefaultComboBoxModel boxModel = new DefaultComboBoxModel();
		for(int i= 0;i<lista.size();i++){
			Asignatura asignatura = lista.get(i);
			boxModel.addElement(asignatura.getCodigo()+"-"+asignatura.getNombre());
		}
		hpBox.setModel(boxModel);
		int n=20;
		ItemListaAyudantiaPanel[] array = new ItemListaAyudantiaPanel[n];
		for(int i=0;i<n;i++){
			//Objeto obj = new Objeto();
			//obj.setNombre("Panel "+(i+1));
			Ayudantia ayudantia = new Ayudantia();
			ayudantia.setId(i);
			ayudantia.setFecha((i+10)+"/10/2019");
			ayudantia.setPrecio(i*20+990);
			array[i] = new ItemListaAyudantiaPanel(ayudantia);
			array[i].setLocation(0, 35*i);
			pnlLista.add(array[i]);
			pnlLista.setPreferredSize(new Dimension(300, (i+1)*35));
		}
	}
}
