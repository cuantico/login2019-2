package cl.ufro.dao;

import java.util.ArrayList;
import java.util.List;

import cl.ufro.bd.Usuario;
import utils.ObjetoBd;
import utils.ObjetoDao;

public class UsuarioDao extends ObjetoDao{
	
	public Usuario find(Usuario registro){
		return (Usuario)super.find(registro);
	}
	public Usuario findByCorreo(Usuario usuario){
		List<ObjetoBd> lista = listAll();
		for(int i=0;i<lista.size();i++){
			Usuario aux = (Usuario)lista.get(i);
			if( aux.getCorreo().equals(usuario.getCorreo()))
				return aux;	
		}
		return null;
	}
	public Usuario findFirst(){
		List<ObjetoBd> lista = listAll();//los elementos de lista van a ser del tipo objetoBd 
		if(lista.size()==1){
			return (Usuario)(lista.get(0));//retorna el primero de la lista
		}else 		
		return null;
	}
	/*
	public List<Usuario> list(){ 
		List<Usuario> usuarios = new ArrayList<Usuario>();
		List<ObjetoBd> lista = listAll();
		for(int i=0;i<lista.size();i++){
			Usuario aux = (Usuario)lista.get(i);
			usuarios.add(aux);
		}
		return usuarios;		
	}*/

}
