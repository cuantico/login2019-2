package cl.ufro.bd;

import java.io.Serializable;

import utils.ObjetoBd;

public class Ayudantia extends ObjetoBd implements Serializable{

	private int id;
	private int precio;
	private String fecha;


	public Ayudantia(){
		addToPrimaryKey("id");
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getId() {
		return id;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getFecha() {
		return fecha;
	}


	public void setPrecio(int precio) {
		this.precio = precio;
	}


	public int getPrecio() {
		return precio;
	}
}
