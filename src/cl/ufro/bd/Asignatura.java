package cl.ufro.bd;

import java.io.Serializable;

import utils.ObjetoBd;

public class Asignatura extends ObjetoBd implements Serializable{

	private String nombre;
	private String codigo;
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigo() {
		return codigo;
	}
	
	
}
