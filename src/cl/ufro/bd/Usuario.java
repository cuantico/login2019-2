package cl.ufro.bd;

import java.io.Serializable;

import utils.ObjetoBd;

public class Usuario extends ObjetoBd implements Serializable{

	private String nombre;
	private String correo;
	private String pin;
	private String rut;

	public Usuario(){
		addToPrimaryKey("correo");
	}


	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String clave) {
		this.pin = clave;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
}
