package cl.ufro.service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import utils.ManagerSession;

import cl.ufro.bd.Usuario;
import cl.ufro.dao.UsuarioDao;
import cl.ufro.solicitud.ActualizarUsuario;
import cl.ufro.solicitud.IdentificarUsuario;
import cl.ufro.solicitud.RegistrarUsuario;

public class UsuarioService {
	
	private UsuarioDao usuarioDao = new UsuarioDao();
	/*
	public Usuario identificar(Usuario usuario){
		Usuario usuarioBD = usuarioDao.findByCorreo(usuario); //buscamos por correo
		if(usuarioBD!=null && usuarioBD.getPin().equals(usuario.getPin()))
			return usuarioBD;
		return null;		
	}*/

	public void actualizar(Usuario usuario) {
		try {
			Socket socket = new Socket("127.0.0.1",5000);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ActualizarUsuario actualizarUsuario = new ActualizarUsuario();
			actualizarUsuario.setCorreo(usuario.getNombre());
			oos.writeObject(actualizarUsuario);
		}catch (Exception e) {e.printStackTrace();}
	}
	public Usuario findByCorreo(Usuario usuario){
		return usuarioDao.findByCorreo(usuario);
	}

	public void guardar(Usuario usuario){
		usuarioDao.add(usuario);
	}
	public Usuario findFirst(){
		return usuarioDao.findFirst();
	}
public void registrar(Usuario usuario){
		
		try {
			Socket socket = new Socket("127.0.0.1",5000);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			RegistrarUsuario registrarUsuario = new RegistrarUsuario();
			registrarUsuario.setCorreo(usuario.getCorreo());
			oos.writeObject(registrarUsuario);
		}catch (Exception e) {e.printStackTrace();}
	}
public Usuario actualizarPin(Usuario usuario){
	Usuario user =(Usuario)ManagerSession.findObject("usuario");
	if (user!=null){
	user.setPin(usuario.getPin());
	ManagerSession.saveObject("usuario",user);
	}
	return user;
}
public Usuario getUsuarioSession(){
	return (Usuario)ManagerSession.findObject("usuario");
}
public Usuario identificar(Usuario usuario){
	try {
		Socket socket = new Socket("127.0.0.1",5000);
		ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		IdentificarUsuario identificarUsuario = new IdentificarUsuario();
		identificarUsuario.setCorreo(usuario.getCorreo());
		identificarUsuario.setPin(usuario.getPin());
		oos.writeObject(identificarUsuario);
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		usuario = (Usuario)ois.readObject();
		
		if(usuario == null)
			ManagerSession.removeObject("usuario");
		else
			ManagerSession.saveObject("usuario", usuario);
			
		return usuario;
	}catch (Exception e) {e.printStackTrace();}
	return null;
}


	
}
